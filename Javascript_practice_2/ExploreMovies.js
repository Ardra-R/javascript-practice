let array = [
    { name: "Encanto", year: "2021", image: "https://www.imdb.com/title/tt2953050/mediaviewer/rm2541025281/?ref_=tt_ov_i" },
    { name: "Toy Story 4", year: "2019", image: "https://www.imdb.com/title/tt1979376/mediaviewer/rm3789906688/?ref_=tt_ov_i" },
    { name: "The Lion King", year: "2019", image: "https://www.imdb.com/title/tt6105098/mediaviewer/rm2458872832/?ref_=tt_ov_i" },
    { name: "The Grinch", year: "2018", image: "https://www.imdb.com/title/tt2709692/mediaviewer/rm2635169280/?ref_=tt_ov_i" },
    { name: "Incredibles 2", year: "2018", image: "https://www.imdb.com/title/tt3606756/mediaviewer/rm985091584/?ref_=tt_ov_i" },
    { name: "Coco ", year: "2017", image: "https://www.imdb.com/title/tt2380307/mediaviewer/rm585455872/?ref_=tt_ov_i" },
    { name: "The Boss Baby", year: "2017", image: "https://www.imdb.com/title/tt3874544/mediaviewer/rm1201810944/?ref_=tt_ov_i" },
    { name: "Zootopia", year: "2016", image: "https://www.imdb.com/title/tt2948356/mediaviewer/rm3116491008/?ref_=tt_ov_i" },
    { name: "Finding Dory", year: "2016", image: "https://www.imdb.com/title/tt2277860/mediaviewer/rm2278496512/?ref_=tt_ov_i" },
    { name: "Moana", year: "2016", image: "https://www.imdb.com/title/tt3521164/mediaviewer/rm618728448/?ref_=tt_ov_i" },
    { name: "Inside Out", year: "2015", image: "https://www.imdb.com/title/tt2096673/mediaviewer/rm3662344960/?ref_=tt_ov_i" },
    { name: "Hotel Transylvania", year: "2012", image: "https://www.imdb.com/title/tt0837562/mediaviewer/rm2262673664/?ref_=tt_ov_i" },
    { name: "Ratatouille", year: "2007", image: "https://www.imdb.com/title/tt0382932/mediaviewer/rm937921792/?ref_=tt_ov_i" },
    { name: "Cars", year: "2006", image: "https://www.imdb.com/title/tt0317219/mediaviewer/rm3794114560/?ref_=tt_ov_i" },
    { name: "Finding Nemo", year: "2003", image: "https://www.imdb.com/title/tt0266543/mediaviewer/rm3313243136/?ref_=tt_ov_i" }
];

const container = document.querySelector(".container");
const moviesContainer = document.querySelector(".movies-container");
const movieWrapper = document.querySelector(".movies-list-wrapper");
const movieRow = document.querySelector(".row");

document.addEventListener("DOMContentLoaded", initialCards);


function initialCards() {
    for (let i = 0; i < array.length; i++) {
        let maindiv = document.createElement("div");
        maindiv.classList.add("card");

        let cardbodydiv = document.createElement("div");
        cardbodydiv.classList.add("card-body", "d-flex", "flex-column");
        maindiv.appendChild(cardbodydiv);

        let cardtitle = document.createElement("h4");
        cardtitle.classList.add("card-title");
        cardtitle.innerText = array[i].name;
        cardbodydiv.appendChild(cardtitle);

        let cardsubtitle = document.createElement("h5");
        cardsubtitle.classList.add("card-text");
        cardsubtitle.innerText = array[i].year;
        cardbodydiv.appendChild(cardsubtitle);

        let seemorebutton = document.createElement("button");
        seemorebutton.classList.add("btn-danger", "btn-lg", "mt-auto");
        let a = document.createElement("a");
        a.classList.add("seemore");
        a.innerText = "See more";
        a.href = array[i].image;
        seemorebutton.appendChild(a);
        
        cardbodydiv.appendChild(seemorebutton);
        movieRow.appendChild(maindiv);
    };

};

function updateCards() {
    let i = array.length - 1;
    let maindiv = document.createElement("div");
    maindiv.classList.add("card");

    let cardbodydiv = document.createElement("div");
    cardbodydiv.classList.add("card-body", "d-flex", "flex-column");
    maindiv.appendChild(cardbodydiv);

    let cardtitle = document.createElement("h4");
    cardtitle.classList.add("card-title");
    cardtitle.innerText = array[i].name;
    cardbodydiv.appendChild(cardtitle);

    let cardsubtitle = document.createElement("h5");
    cardsubtitle.classList.add("card-text");
    cardsubtitle.innerText = array[i].year;
    cardbodydiv.appendChild(cardsubtitle);

    let seemorebutton = document.createElement("button");
    seemorebutton.classList.add("btn-danger", "btn-lg", "mt-auto");
    let a = document.createElement("a");
    a.classList.add("seemore");
    a.innerText = "See more";
    a.href = array[i].image;
    seemorebutton.appendChild(a);
    cardbodydiv.appendChild(seemorebutton);
    movieRow.appendChild(maindiv);

}
//add movies modal
const addButton = document.querySelector(".add-button");
const modal = document.querySelector(".modal");
const modaltitle = document.querySelector(".modal-title");
const modalBody = document.querySelector(".modal-body");
const closeButton = document.querySelector(".btn-close");
const submitButton = document.querySelector("#submitBtn");
const form = document.getElementById('addData');
let moviename = "";
let movieyear = "";
let movieimage = "";
addButton.addEventListener('click', function () {
    modal.style.display = "block";
});
closeButton.addEventListener('click', function () {
    modal.style.display = "none";
});
submitButton.addEventListener("click", function (e) {
    e.preventDefault();
    moviename = form.elements["name"].value;
    movieyear = form.elements["movie-year"].value;
    movieimage = form.elements["imageUrl"].value;

    array.push({ name: `${moviename}`, year: movieyear, image: movieimage });
    updateCards();
    modal.style.display = "none";
})


//pagination
const dropdown = document.querySelector(".dropdown");
const dropdownButtonValue = document.querySelector(".dropdown-item");
const dropdownButton = document.querySelector(".dropdown-toggle");

let selText = '';//number per page
$(".dropdown-menu").on('click', 'li a', function () {
    selText = $(this).text();
    $(this).parents('.dropdown').find('.dropdown-toggle').html(selText + ' <span class="caret"></span>');
    displayPage();
});


let pageList = [];
let currentPage = 1;
let numberOfPages = 1;
let pageitem = document.querySelector(".page-item");
let pagelinkprevious = document.querySelector(".prev");
let pagelinknext = document.querySelector(".next");
const ul = document.querySelector(".pag");

function displayPage() {
    numberOfPages = Math.ceil(array.length / selText);
    pageList = array.slice(0, selText);
    movieRow.innerHTML = "";
    for (let i = 0; i < selText; i++) {
        let maindiv = document.createElement("div");
        maindiv.classList.add("card");

        let cardbodydiv = document.createElement("div");
        cardbodydiv.classList.add("card-body", "d-flex", "flex-column");
        maindiv.appendChild(cardbodydiv);

        let cardtitle = document.createElement("h4");
        cardtitle.classList.add("card-title");
        cardtitle.innerText = pageList[i].name;
        cardbodydiv.appendChild(cardtitle);

        let cardsubtitle = document.createElement("h5");
        cardsubtitle.classList.add("card-text");
        cardsubtitle.innerText = pageList[i].year;
        cardbodydiv.appendChild(cardsubtitle);

        let seemorebutton = document.createElement("button");
        seemorebutton.classList.add("btn-danger", "btn-lg", "mt-auto");
        let a = document.createElement("a");
        a.classList.add("seemore");
        a.innerText = "See more";
        a.href = pageList[i].image;
        seemorebutton.appendChild(a);
        cardbodydiv.appendChild(seemorebutton);
        movieRow.appendChild(maindiv);
    }
}

let noofitems = array.length;
numberOfPages = Math.ceil(noofitems / selText);

pagelinkprevious.addEventListener("click", function () {
    currentPage = currentPage - 1;
    let start = ((currentPage - 1) * selText);
    console.log(start);
    let end = start + +selText;
    console.log(end);
    pageList = array.slice(start, end);
    console.log(pageList);
    movieRow.innerHTML = "";
    for (let i = 0; i < pageList.length; i++) {
        let maindiv = document.createElement("div");
        maindiv.classList.add("card");

        let cardbodydiv = document.createElement("div");
        cardbodydiv.classList.add("card-body", "d-flex", "flex-column");
        maindiv.appendChild(cardbodydiv);

        let cardtitle = document.createElement("h4");
        cardtitle.classList.add("card-title");
        cardtitle.innerText = pageList[i].name;
        cardbodydiv.appendChild(cardtitle);

        let cardsubtitle = document.createElement("h5");
        cardsubtitle.classList.add("card-text");
        cardsubtitle.innerText = pageList[i].year;
        cardbodydiv.appendChild(cardsubtitle);

        let seemorebutton = document.createElement("button");
        seemorebutton.classList.add("btn-danger", "btn-lg", "mt-auto");
        let a = document.createElement("a");
        a.classList.add("seemore");
        a.innerText = "See more";
        a.href = pageList[i].image;
        seemorebutton.appendChild(a);
        cardbodydiv.appendChild(seemorebutton);
        movieRow.appendChild(maindiv);
    }
    check();
    // }


});
pagelinknext.addEventListener("click", nextpage);

function nextpage() {
    currentPage = currentPage + 1;
    let start = ((currentPage - 1) * selText);
    console.log(start);
    let end = start + +selText;
    console.log(end);
    pageList = array.slice(start, end);
    movieRow.innerHTML = "";
    for (let i = 0; i < pageList.length; i++) {
        let maindiv = document.createElement("div");
        maindiv.classList.add("card");

        let cardbodydiv = document.createElement("div");
        cardbodydiv.classList.add("card-body", "d-flex", "flex-column");
        maindiv.appendChild(cardbodydiv);

        let cardtitle = document.createElement("h4");
        cardtitle.classList.add("card-title");
        cardtitle.innerText = pageList[i].name;
        cardbodydiv.appendChild(cardtitle);

        let cardsubtitle = document.createElement("h5");
        cardsubtitle.classList.add("card-text");
        cardsubtitle.innerText = pageList[i].year;
        cardbodydiv.appendChild(cardsubtitle);

        let seemorebutton = document.createElement("button");
        seemorebutton.classList.add("btn-danger", "btn-lg", "mt-auto");
        let a = document.createElement("a");
        a.classList.add("seemore");
        a.innerText = "See more";
        a.href = pageList[i].image;
        seemorebutton.appendChild(a);
        cardbodydiv.appendChild(seemorebutton);
        movieRow.appendChild(maindiv);
    }
    check();
    // }

};

function check() {
    document.getElementById("Next").disabled = currentPage == numberOfPages ? true : false;
    document.getElementById("Prev").disabled = currentPage == 1 ? true : false;
}
