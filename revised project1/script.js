let array = ['Toy Story','Finding Nemo','Incredibles','Moana','Ratatouille','101 Dalmatians','The Lion King',]

//selectors
const dataInput = document.querySelector(".data-input");
const addButton = document.querySelector(".add-button");
const dataList = document.querySelector(".data-list");

//event listeners
document.addEventListener("DOMContentLoaded",initialArray);
addButton.addEventListener("click",addData);
dataList.addEventListener("click",deleteData);

//functions
function initialArray(){
    for(let i=0;i<array.length;i++){
        let listDiv = document.createElement("div");
        listDiv.classList.add("main-list");
        let li = document.createElement("li");
        li.classList.add("main-data");
        li.innerText = array[i];
        listDiv.appendChild(li);
        let button = document.createElement("button");
        button.classList.add("btn-danger");
        button.innerText = "Delete"
        listDiv.appendChild(button);
        dataList.appendChild(listDiv);
    }
}

function addData(e){
    e.preventDefault();
    let listDiv = document.createElement("div");
    listDiv.classList.add("main-list");
    let li = document.createElement("li");
    li.classList.add("main-data");
    li.innerText = dataInput.value;
    listDiv.appendChild(li);
    
    let button = document.createElement("button");
    button.classList.add("btn-danger");
    button.innerText = "Delete"
    listDiv.appendChild(button);
    dataList.appendChild(listDiv);
    array.push(dataInput.value);
    console.log(array);
}


function deleteData(e){
    //console.log(dataList.children);
    e.target.parentNode.remove();
    array.splice(array.indexOf(`${e.target.previousSibling.innerText}`),1);
    console.log(array);
    //console.log(e.target.previousSibling.innerText);
    
}
