let list = ['Toy Story','Finding Nemo','Incredibles','Moana','Ratatouille','101 Dalmatians','The Lion King',
'Aladdin','Cars','Frozen','Despicable Me','Zootopia','Pinocchio','Dumbo','Bolt'];

let tag = "";
let button = "";
function displayNames(){
    
    for(let i=0; i<list.length; i++){
        button =`<button class=button-${i} onClick=deleteElement(${i})>Delete</button>`;
        tag = tag+ `<ul class=names-${i}>` + list[i] +' '+button+ '</ul>';
        document.getElementById('arraydata').innerHTML = tag;
        document.getElementsByClassName(`.button-${i}`).innerHTML = button;
    };
};

function deleteElement(i){
    //list.splice(i,1);
    
    document.querySelector(`.names-${i}`).innerHTML = " ";
    document.getElementsByClassName(`.button-${i}`).innerHTML = " ";

    //console.log(list);
}

displayNames();